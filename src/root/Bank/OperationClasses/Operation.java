package root.Bank.OperationClasses;

import java.io.Serializable;

public class Operation implements Serializable {
	private int operationId, srcAccountId, destAccountId, accountId, clientId;
	private double amount, operationCost;
	private String accountType, operationType;

    public Operation(int operationId, int srcAcccountId, int destAccountId, double amount, double operationCost, String operationType) {
    	this.operationId = operationId;
    	this.srcAccountId = srcAcccountId;
    	this.destAccountId = destAccountId;
    	this.amount = amount;
    	this.operationCost = operationCost;
    	this.operationType = operationType;
	}
    
    public Operation(int operationId, int accountId, int clientId, String accountType, String operationType) {
    	this.operationId = operationId;
    	this.accountId = accountId;
    	this.clientId = clientId;
    	this.accountType = accountType;
    	this.operationType = operationType;
	}
    
    @Override
    public String toString() {
    	return operationId + " " + srcAccountId + " " + destAccountId + " " + amount + " " + operationCost + " " + operationType;
    }
    
    public int getOperationId() {
		return operationId;
	}
}
