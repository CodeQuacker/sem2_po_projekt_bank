package root.Bank.OperationClasses;

import java.io.*;
import java.util.ArrayList;

public class Operations {
	private static int lastFreeId = 0;
    private static ArrayList<Operation> operations = new ArrayList<>();

	public static void saveOperations() throws IOException, FileNotFoundException {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data/operations.dat"));
		out.writeObject(operations);
		out.close();

		ObjectOutputStream outId = new ObjectOutputStream(new FileOutputStream("data/operationsId.dat"));
		outId.writeObject(lastFreeId);
		outId.close();
	}

	public static void readOperations() throws IOException, ClassNotFoundException, FileNotFoundException {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("data/operations.dat"));
		operations = (ArrayList<Operation>) in.readObject();
		in.close();

		ObjectInputStream inId = new ObjectInputStream(new FileInputStream("data/operationsId.dat"));
		lastFreeId = (int) inId.readObject();
		inId.close();
	}

    public static void addNewOperation(int srcAccountId, int destAccountId, double amount, double operationCost, String operationType) {
    	operations.add(new Operation(lastFreeId, srcAccountId, destAccountId, amount, operationCost, operationType));
    	lastFreeId++;
    }
    
    public static void addNewOperation2(int accountId, int clientId, String accountType, String operationType) {
    	operations.add(new Operation(lastFreeId, accountId, clientId, accountType, operationType));
    	lastFreeId++;
    }

    public static Operation getOperationInfo(int operationId) {
    	if(operations.isEmpty()) {
			System.out.print("The list is empty!");
		}
    	else {
    		for(int index = 0; index < operations.size(); index++) {
    			if(operations.get(index).getOperationId() == operationId) {
    				return operations.get(index);
    			}
    		}
    		System.out.print("There is no such operation!");
		}
        return null;
    }
}
