package root.Bank.Exceptions;

public class InsufficientFunds extends IndexOutOfBoundsException{
    public InsufficientFunds() {}
    public InsufficientFunds(String message) {super(message);}
}
