package root.Bank.AccountClasses;

import root.Bank.ClientClasses.Clients;

import java.io.Serializable;

public abstract class Account implements Serializable {
    protected String name;
    protected int accountId, clientId;
    private double balance;
    protected double interest, operationCost;

    public void changeName(String name){
        this.name = name;
    }

    // ======================================== Get/Set ================================================


    public String getName() {
        return name;
    }

    public int getAccountId() {
        return accountId;
    }

    public int getClientId() {
        return clientId;
    }

    public double getBalance(){
        return balance;
    }

    protected void setBalance(double balance) {
        this.balance = balance;
    }

    public double getInterest() {
        return interest;
    }

    public double getOperationCost() {
        return operationCost;
    }

    public String toString(String accountType) {
        return "Account \"" + name + "\", Account type: " + accountType +
                "\n    Owner: " + Clients.getClientInfo(clientId) +
                "\n    Balance=: " + balance + " PLN" +
                "\n    Interest: " + interest*100 +"%, Operation cost: " + operationCost*100 + "%" +
                "\n    accountId: " + accountId + ", clientId: " + clientId;
    }
}
