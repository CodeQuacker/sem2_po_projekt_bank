package root.Bank.AccountClasses;

public class Standard extends Account{
    public Standard(int clientId, int accountId, String name){
        this.name = name;
        this.accountId = accountId;
        this.clientId = clientId;
        interest = 0;
        operationCost = 0;
    }

    @Override
    public String toString() {
        return super.toString("Standard");
    }
}
