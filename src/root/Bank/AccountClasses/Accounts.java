package root.Bank.AccountClasses;

import java.io.*;
import java.util.ArrayList;

public class Accounts {
    private static int lastFreeId = 1;
    private static ArrayList<Account> accounts = new ArrayList<>();

    public static void saveAccounts() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data/accounts.dat"));
        out.writeObject(accounts);
        out.close();

        ObjectOutputStream outId = new ObjectOutputStream(new FileOutputStream("data/accountsId.dat"));
        outId.writeObject(lastFreeId);
        outId.close();
    }

    public static void readAccounts() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data/accounts.dat"));
        accounts = (ArrayList<Account>) in.readObject();
        in.close();

        ObjectInputStream inId = new ObjectInputStream(new FileInputStream("data/accountsId.dat"));
        lastFreeId = (int) inId.readObject();
        inId.close();
    }


    protected static Account findAccount(int accountId){
        Account output = null;
        for (Account acc : accounts) {
            if (acc.getAccountId() == accountId) output = acc;
        }
        return output;
    }

    protected static ArrayList<Account> getAllClientAccounts(int clientId){
        ArrayList<Account> output = new ArrayList<>();
        for (Account acc : accounts) {
            if (acc.clientId == clientId) output.add(acc);
        }
        return output;
    }

    protected static void addNewAccount(int clientId, int type, String name){
        Account newAccount;

        if (type == 1) {
            newAccount = new Standard(clientId, lastFreeId, name);
        }else if (type == 2){
            newAccount = new Saving(clientId, lastFreeId, name);
        }else{
            throw new IllegalArgumentException("Wrong account type");
            // TODO: 03.06.2021 custom exception
        }

        lastFreeId++;
        if (newAccount != null) accounts.add(newAccount);
    }

    protected static void removeAccount(int accountId){
        accounts.removeIf(acc -> acc.getAccountId() == accountId);
    }

    protected static void setBalance(int accountId, double amount){
        for (Account acc : accounts) {
            if (acc.getAccountId() == accountId) acc.setBalance(amount);
        }
    }

    protected static ArrayList<Account> getAccounts() {
        return accounts;
    }
}
