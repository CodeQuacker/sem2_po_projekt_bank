package root.Bank.AccountClasses;

import root.Bank.ClientClasses.Client;
import root.Bank.Exceptions.InsufficientFunds;
import root.Bank.OperationClasses.Operations;
import java.util.ArrayList;
import java.util.Scanner;

public class AccountUtils {
    private final static Scanner sc = new Scanner(System.in);

    //                  ********************************************************
    //                  *                      ADD/REMOVE                      *
    //                  ********************************************************

    public static void createAccount(int clientId){
        String name = getString("Enter account name (without spaces): ");
        int type = getInt("Choose account type:\n 1 - Standard account\n 2 - Saving account\n");

        if (type == 0) {
            System.out.println("Canceling operation");
            return;
        }else if (type == 1 || type == 2) {
            Accounts.addNewAccount(clientId, type, name);
            System.out.println("Account created\n");
        }else {
            System.out.println("Unrecognized option");
        }

    }

    public static void removeAccount(int clientId){
        if (!printClientAccounts(clientId)) return;
        int choice = getInt("Enter id of account you want to delete");
        if (choice == 0) {
            System.out.println("Canceling operation");
            return;
        }
        for (Account acc : Accounts.getAllClientAccounts(clientId)) {
            if (acc.getAccountId() == choice) {
                Accounts.removeAccount(choice);
                System.out.println("Account removed\n");
                return;
            }
        }
        System.out.println("There is no account with that id! Stopping operation.");
    }

    //                  ********************************************************
    //                  *                      OPERATIONS                      *
    //                  ********************************************************

    /**
     * Transfers (if possible) money from source account to destination account
     */
    public static void transfer(int srcClientId){
        System.out.println("Your accounts:");
        if (!printClientAccounts(srcClientId)) return;
        int srcAccountId = getInt("Please enter source account ID");
        if (!accountExists(srcAccountId)) return;
        int dstAccountId = getInt("Please enter the destination account ID");
        if (!accountExists(dstAccountId)) return;
        double amount = getDouble("Please enter the amount");
        if (amount == 0) return;

        Account src = Accounts.findAccount(srcAccountId);
        Account dst = Accounts.findAccount(dstAccountId);
        double opCost;
        if (src.getOperationCost() != 0) opCost = amount * src.getOperationCost();
        else opCost = 0;

        if (src.getBalance() >= (amount + opCost)) {
            Accounts.setBalance(srcAccountId, src.getBalance() - (amount + opCost));
            Accounts.setBalance(dstAccountId, dst.getBalance() + amount);
            System.out.println("Transfered " + amount + " PLN");
            if (opCost != 0) System.out.println("Operation cost: " + opCost + " PLN");
            Operations.addNewOperation(srcAccountId, dstAccountId, amount, src.operationCost, "Transfer");
        }
        else {
            double rest = (amount + opCost) - src.getBalance();
            throw new InsufficientFunds("Current balance: " + src.getBalance() + ", you are " + rest + " short.");
        }
    }

    public static void deposit(int srcClientId){
        System.out.println("Your accounts:");
        if (!printClientAccounts(srcClientId)) return;
        int srcAccountId = getInt("Please enter your account ID");
        if (!accountExistsForClient(srcAccountId, srcClientId)) return;
        double amount = getDouble("Please enter the amount");
        if (amount == 0) return;

        Account src = Accounts.findAccount(srcAccountId);

        Accounts.setBalance(srcAccountId, src.getBalance() + amount);
        System.out.println("Deposited " + amount + " PLN");
        Operations.addNewOperation(srcAccountId, 0, amount, src.operationCost, "Deposit");
    }

    public static void withdraw(int srcClientId){
        System.out.println("Your accounts:");
        if (!printClientAccounts(srcClientId)) return;
        int srcAccountId = getInt("Please enter your account ID");
        if (!accountExistsForClient(srcAccountId, srcClientId)) return;
        double amount = getDouble("Please enter the amount");
        if (amount == 0) return;

        Account src = Accounts.findAccount(srcAccountId);
        double opCost;
        if (src.getOperationCost() != 0) opCost = amount * src.getOperationCost();
        else opCost = 0;

        if (src.getBalance() >= (amount + opCost)) {
            Accounts.setBalance(srcAccountId, src.getBalance() - (amount + opCost));
            System.out.println("Withdrawn " + amount + " PLN");
            if (opCost != 0) System.out.println("Operation cost: " + opCost + " PLN");
            Operations.addNewOperation(srcAccountId, 0, amount, src.operationCost, "Withdraw");
        }
        else {
            double rest = (amount + opCost) - src.getBalance();
            throw new InsufficientFunds("Current balance: " + src.getBalance() + ", you are " + rest + " short.");
        }
    }

    //                  ********************************************************
    //                  *                        OUTPUTS                       *
    //                  ********************************************************

    /**
     * Asks for accountId and prints info about said account
     */
    public static void printAccountInfo(){
        int accountId = getInt("Please enter your account ID");
        if(accountExists(accountId)) {
            System.out.println(Accounts.findAccount(accountId));
        }
    }

    public static void findAccountByString(String input){
        int printed = 0;
        for (Account acc : Accounts.getAccounts()) {
            if(acc.getName().toLowerCase().contains(input.toLowerCase())) {
                System.out.println("Name:" + acc.getName() + ", Account Id: " + acc.getAccountId());
                printed++;
            }
        }
        if (printed == 0) System.out.println("There are no accounts matching your input");
    }

    /**
     * Prints all client accounts based on provided clientId
     * Works as boolean too!!!
     * @param clientId provided client id
     * @return boolean - (true) if client has at least 1 account, (false) if client has no accounts
     */
    public static boolean printClientAccounts(int clientId){
        ArrayList<Account> clientAccounts = Accounts.getAllClientAccounts(clientId);
        if (clientAccounts.size() != 0){
            for (Account acc : clientAccounts) {
                System.out.println("Name:" + acc.getName() + ", Account Id: " + acc.getAccountId());
            }
            return true;
        }else {
            System.out.println("This client has no accounts.\n");
            return false;
        }
    }

    /**
     * Check if account exists. Prints "Unrecognized account ID" if false.
     * @param accountId account to check
     * @return true if exists, false if not
     */
    public static boolean accountExists(int accountId){
        if (Accounts.findAccount(accountId) != null) return true;
        System.out.println("Unrecognized account ID");
        return false;
    }

    public static boolean accountExistsForClient(int accountId, int clientId){
        Account acc = Accounts.findAccount(accountId);
        if (acc == null || acc.getClientId() != clientId) {
            System.out.println("There is no such account assigned to you.");
            return false;
        }
        return true;
    }


    
    //                  ********************************************************
    //                  *                         MISC                         *
    //                  ********************************************************

    /**
     * Loops until you get an int
     * @param messsage printed out before input
     * @return int
     */
    private static int getInt(String messsage){
        int output;
        while (true){
            try {
                System.out.println(messsage + " (0 to cancel): ");
                output = sc.nextInt();
                return output;
            } catch (Exception e) {
                System.out.println("Not a number. Try again.");
                sc.next();
            }
        }
    }

    /**
     * Loops until you get an double
     * @param messsage printed out before input
     * @return double
     */
    private static double getDouble(String messsage){
        double output;
        while (true){
            try {
                System.out.println(messsage + " (0 to cancel): ");
                output = sc.nextDouble();
                return output;
            } catch (Exception e) {
                System.out.println("Not a number. Try again.");
                sc.next();
            }
        }
    }

    private static String getString(String messsage){
        System.out.println(messsage);
        String output = sc.next();
        return output;
    }
    
    public static int getNumberOfAccounts(int clientId) {
    	return Accounts.getAllClientAccounts(clientId).size();
    }
}
