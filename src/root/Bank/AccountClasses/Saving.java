package root.Bank.AccountClasses;

public class Saving extends Account{
    public Saving(int clientId, int accountId, String name){
        this.name = name;
        this.accountId = accountId;
        this.clientId = clientId;
        interest = 0.0125;
        operationCost = 0.02;
    }

    @Override
    public String toString() {
        return super.toString("Saving");
    }
}
