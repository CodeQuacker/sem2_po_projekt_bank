package root.Bank.ClientClasses;

import java.io.Serializable;

public class Client implements Serializable {
    private int clientId;
    private String name, surname;

    public Client(int clientId, String name, String surname){
    	this.clientId = clientId;
    	this.name = name;
    	this.surname = surname;
    }
    
    @Override
    public String toString() {
    	return "Id:" + clientId + ", " + name + " " + surname;
    }
    
    public int getClientId() {
		return clientId;
	}

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}