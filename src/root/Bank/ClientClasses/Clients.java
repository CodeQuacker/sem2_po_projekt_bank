package root.Bank.ClientClasses;

import java.io.*;
import java.util.ArrayList;

public class Clients implements Serializable {
    private static int lastFreeId = 1;
    private static ArrayList<Client> clients = new ArrayList<>();

    public static void saveClients() throws IOException {
		ObjectOutputStream outArray = new ObjectOutputStream(new FileOutputStream("data/clients.dat"));
		outArray.writeObject(clients);
		outArray.close();

		ObjectOutputStream outId = new ObjectOutputStream(new FileOutputStream("data/clientsId.dat"));
		outId.writeObject(lastFreeId);
		outId.close();
	}

	public static void readClients() throws IOException, ClassNotFoundException {
		ObjectInputStream inArray = new ObjectInputStream(new FileInputStream("data/clients.dat"));
		clients = (ArrayList<Client>) inArray.readObject();
		inArray.close();

		ObjectInputStream inId = new ObjectInputStream(new FileInputStream("data/clientsId.dat"));
		lastFreeId = (int) inId.readObject();
		inId.close();
	}

    public static void addNewClient(String name, String surname) {
    	clients.add(new Client(lastFreeId, name, surname));
    	lastFreeId++;
    }

    public static void removeClient(int clientId)  {
    	boolean removed = false;
    	if(clients.isEmpty()) {
			System.out.print("The list is empty!");
		}
    	else {
    		for(int index = 0; index < clients.size(); index++) {
    			if(clients.get(index).getClientId() == clientId) {
    				clients.remove(index);
    				removed = true;
    			}
    		}
    		if(!removed) {
    			System.out.print("There is no such client!");
    		}
		}
    }

    public static ArrayList<Client> getAllClients() {
        if(clients.isEmpty()) {
			return null;
		}
        else {
        	return clients;
		}
    }

    public static void findClientByString(String input){
		int printed = 0;
    	for (Client client : clients) {
			if(client.getName().toLowerCase().contains(input.toLowerCase()) || client.getSurname().toLowerCase().contains(input.toLowerCase())) {
				System.out.println(client);
				printed++;
			}
		}
		if (printed == 0) System.out.println("There are no accounts matching your input");
	}

    public static void printClientList(int clientId){
		System.out.println("List of all other clients:");
		for (Client client:clients) {
			if (client.getClientId() != clientId) {
				System.out.println(client);
			}
		}

	}



    public static Client getClientInfo(int clientId) {
    	if(clients.isEmpty()) {
		}
    	else {
    		for(int index = 0; index < clients.size(); index++) {
    			if(clients.get(index).getClientId() == clientId) {
    				return clients.get(index);
    			}
    		}
		}
    	return null;
    }

    public static int getLastFreeid() {
    	return lastFreeId;
    }

	public static void printClients() {
		for (Client client:clients) {
			System.out.println(client.toString());
		}
	}
}
