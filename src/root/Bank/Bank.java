package root.Bank;

import java.io.IOException;
import java.util.Scanner;
import root.Bank.AccountClasses.AccountUtils;
import root.Bank.AccountClasses.Accounts;
import root.Bank.ClientClasses.Clients;
import root.Bank.Exceptions.InsufficientFunds;
import root.Bank.OperationClasses.Operations;


public class Bank {
	
	private static Scanner sc = new Scanner(System.in);
	
    public static void bankSave() throws IOException {
        Clients.saveClients();
		Accounts.saveAccounts();
		Operations.saveOperations();
    }

    public static void bankLoad() {
		try {
			Clients.readClients();
			Accounts.readAccounts();
			Operations.readOperations();
			System.out.println("Saved data found and loaded.");
		}catch (Exception e) {
			System.out.println("No saved data, starting new Bank app.");
		}
	}
    
    public static void start() throws IOException  {
		bankLoad();


    	int clientId = 0;
    	System.out.println("Welcome to our bank!");
    	
    	int option;
		while (true){
    		System.out.println("1: New client\n"
							 + "2: Remove client\n"
							 + "3: Existing client\n"
							 + "4: Exit");
			try {
    			option = sc.nextInt();
			} catch (Exception e) {
				option = 0;
				sc.next();
			}
			switch (option) {
				case 1:
					System.out.print("Enter your name (Without spaces):\n");
					String name = sc.next();
					System.out.print("Enter your surname (Without spaces):\n");
					String surname = sc.next();
					System.out.print("Your client ID is: " + Clients.getLastFreeid() +"\n");
					Clients.addNewClient(name, surname);
					break;

				case 2:
					while (true) {
						if (Clients.getAllClients().size() == 0) {
							System.out.println("No accounts to remove");
							break;
						}
						Clients.printClients();
						System.out.print("Choose client ID of an account you want to remove (0 to cancel):");
						try {
							clientId = sc.nextInt();
							if (clientId == 0) break;
							if (Clients.getClientInfo(clientId) != null) {
								System.out.println("Removed " + Clients.getClientInfo(clientId));
								Clients.removeClient(clientId);
								break;
							} else {
								System.out.println("Unrecognized Client ID. Please re-enter\n");
							}

						} catch (Exception e) {
							System.out.println("Unrecognized Client ID. Please re-enter\n");
							sc.next();
						}
					}
					break;

				case 3:
					while (true) {
						if (Clients.getAllClients() == null) {
							System.out.println("No accounts to log in");
							break;
						}
						Clients.printClients();
						System.out.print("Please provide your client ID (0 to cancel):");
						try {
							clientId = sc.nextInt();
							if (clientId == 0) break;
							if (Clients.getClientInfo(clientId) != null) {
								System.out.println("Logged in " + Clients.getClientInfo(clientId));
								menu(clientId);
								break;
							} else {
								System.out.println("Unrecognized Client ID. Please re-enter\n");
							}

						} catch (Exception e) {
							System.out.println("Unrecognized Client ID. Please re-enter\n");
							sc.next();
						}
					}
					break;

				case 4:
					return;
				default:
					System.out.print("Unrecognized option. Try again.\n");
			}
			bankSave();
		}
    }
    
    public static void menu(int clientId) throws IOException  {
    	
    	int option;
    	boolean shouldContinue = true;
    	while(shouldContinue) {
			System.out.println("\nWhat would you like to do now?\n"
								+ "1: Create a new account\n"
								+ "2: Show your account\n"
								+ "3: Remove your accont\n"
								+ "4: Transfer funds from your account\n"
								+ "5: Withdraw funds from your account\n"
								+ "6: Deposit funds to your account\n"
								+ "7: Find client\n"
								+ "8: Check client\n"
								+ "9: Find Account\n"
								+ "0: Log out");
			try {
    			option = sc.nextInt();
			} catch (Exception e) {
				option = 0;
				sc.next();
			}
			switch(option) {
    			case 1:	AccountUtils.createAccount(clientId);
    					break;
    			
    			case 2: if (!AccountUtils.printClientAccounts(clientId)) break;
    					AccountUtils.printAccountInfo();
    					break;
						
    			case 3:	AccountUtils.removeAccount(clientId);
    					break;
    					
    			case 4: try{
							AccountUtils.transfer(clientId);
						}catch (InsufficientFunds e) {
							System.out.println("Insufficient funds, " + e.getMessage());
						}
    					break;

    			case 5: try{
							AccountUtils.withdraw(clientId);
						}catch (InsufficientFunds e) {
							System.out.println("Insufficient funds, " + e.getMessage());
						}
						break;

    			case 6:	AccountUtils.deposit(clientId);
    					break;

				case 7: System.out.println("Enter client name or surname (not both) you want to find: ");
						String choiceCli = sc.next();
						Clients.findClientByString(choiceCli);
						break;

				case 8: Clients.printClientList(clientId);
						System.out.println("Enter ID of a client you wish to check: ");
						int checkId;
						try {
							checkId = sc.nextInt();
						} catch (Exception e) {
							checkId = 0;
							sc.next();
						}
						if (checkId == 0) {
							System.out.println("Wrong ID or not an ID");
							break;
						}
						AccountUtils.printClientAccounts(checkId);
						break;

				case 9: System.out.println("Enter account name you want to find: ");
						String choiceAcc = sc.next();
						AccountUtils.findAccountByString(choiceAcc);
						break;

    			case 0:	shouldContinue = false;
    					break;
    			
    			default:
    					System.out.println("Unrecognized option, please re-enter");
			}
			bankSave();
    	}
    }
}
